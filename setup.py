from setuptools import setup, find_packages

setup(
    name='desfrene_dyndns',
    version='1.0',
    platforms='any',
    url='https://gitlab.com/desfrene/desfrene-dyndns',
    license='GPL-3',
    author='Gabriel Desfrene',
    author_email='desfrene.gabriel@gmail.com',
    description='Desfrene DynDns',
    packages=find_packages(),
    entry_points='''[console_scripts]
desfrene-dyndns=desfrene_dyndns:main''',
)
