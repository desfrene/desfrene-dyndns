import argparse
import json
import sys
from pathlib import Path

import requests

requests.packages.urllib3.util.connection.HAS_IPV6 = False
config_file = Path(__file__).parent.resolve() / "config.json"


def update_dns() -> bool:
    with config_file.open('r') as f:
        config = json.load(f)

    hostname = config["hostname"]
    user = config["user"]
    password = config["password"]

    answer = requests.get(
        "https://www.ovh.com/nic/update?system=dyndns&hostname={}".format(hostname),
        auth=(user, password)).text.strip()

    myip = requests.get("https://ifconfig.ovh").text.strip()

    if "nohost" in answer:
        print("Bad configuration detected.\n"
              f"Unable to find {hostname} host.\n"
              "Please check hostname.")
        config_file.unlink()
        return True

    elif "Authorization Required" in answer:
        print("Bad configuration detected.\n"
              f"Unable to login with the user : {user}.\n"
              "Please check credentials.")
        config_file.unlink()
        return True

    elif "!yours" in answer:
        print("Bad configuration detected.\n"
              f"Unable to update {hostname} with the user : {user}.\n"
              "Please check user access.")
        config_file.unlink()
        return True

    elif (myip in answer) and ("nochg" in answer):
        print(f"Keeping {myip}...")
        return False

    elif (myip in answer) and ("good" in answer):
        print(f"Updating ip to {myip}...")
        return False


def setup_config() -> None:
    config_file.unlink(missing_ok=True)

    print("--------------------------------------------------")
    print("         Configuration of Desfrene DynDns         ")
    print("--------------------------------------------------")
    print()

    hostname = input("Enter hostname : ")
    print()

    user = input("Enter user : ")
    print()

    password = input("Enter password : ")
    print()

    with config_file.open('w') as f:
        json.dump({"hostname": hostname,
                   "user": user,
                   "password": password}, f)

    if update_dns():
        print("To fix issue please run :\n"
              f"{sys.argv[0]} --setup\n")
        exit(1)
    else:
        print("Configuration saved !")


def main() -> None:
    parser = argparse.ArgumentParser(description='Update your OVH DNS with Desfrene DynDns.')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--setup', action='store_true', help='setup configuration.')
    group.add_argument('--update', action='store_true', help='update configured hostname ip.')

    args = parser.parse_args()

    if args.setup:
        setup_config()
    elif args.update and config_file.exists():
        update_dns()
    else:
        print("No configuration detected. Please run :\n"
              f"{sys.argv[0]} --setup")
